package com.isobar;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/contact")
public class ContactController {
	
	@Autowired
	ContactService contactService;
	
	@PostMapping("/create")
	public Contact createContact(@RequestBody Contact contact) {
		return contactService.createContact(contact);
	}
	
	@PutMapping("/edit")
	public Contact editContact(@RequestBody Contact contact) {
		return contactService.editContact(contact);
	}
	
	@DeleteMapping("/delete/{id}")
	public void deleteContact(@PathVariable Long id) {
		contactService.deleteContact(id);
	}
	
	@GetMapping("/get")
	public List<Contact> getContacts(@RequestParam(defaultValue = "0") Integer pageNo, @RequestParam(defaultValue = "10") Integer pageSize) {
		return contactService.getContacts(pageNo, pageSize);
	}

}
