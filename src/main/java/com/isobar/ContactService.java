package com.isobar;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class ContactService {
	
	@Autowired
	ContactDao contactDao;
	
	public Contact createContact(Contact contact) {
		return contactDao.save(contact);
	}
	
	public Contact editContact(Contact contact) {
		Contact updatedContact = new Contact();
		if (contactDao.existsById(contact.getId())) {
			updatedContact = contactDao.save(contact);
		}
		return updatedContact;
	}
	
	public void deleteContact(Long id) {
		if (contactDao.existsById(id)) {
			contactDao.deleteById(id);
		}
	}
	
	public List<Contact> getContacts(Integer pageNo, Integer pageSize) {
		Pageable paging = PageRequest.of(pageNo, pageSize);
		Page<Contact> pagedResult = contactDao.findAll(paging);
		if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<Contact>();
        }
	}

}
